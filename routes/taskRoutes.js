
const express = require("express");

const router = express.Router();

const taskController = require("../controllers/taskControllers.js");

router.get("/", (req, res) => {

	taskController.getAllTasks().then(resultFromController => {res.send(resultFromController)});
});

router.post("/create", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => {res.send(resultFromController)});
});

router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => {res.send(resultFromController)});
})

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => {res.send(resultFromController)});
})

// ACTIVITY SESSION 36

router.get("/:id", (req, res) => {

	taskController.getTask(req.params.id, req.body).then(resultFromController => {res.send(resultFromController)});
})

router.put("/:id/complete", (req, res) => {
	taskController.updatedTaskStatus(req.params.id, req.body).then(resultFromController => {res.send(resultFromController)});
})

module.exports = router;




// Routes is used to get one url to another with the use of express
// This contains all the URI endpoints for our application

//const express = require("express");
// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
//const router = express.Router();
//const taskController = require("../controllers/taskControllers.js")

// [SECTION] Routes
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
// They invoke the controller functions from the controller files
// All the business logic is done in the controller
/*router.get("/", (req, res) => {

	taskController.getAllTasks().then(resultFromController => {res.send (
		resultFromController)});
})

router.post("/create", (req, res) => {

	taskController.createTask(req.body).then(resultFromController => {res.send(resultFromController)});
});
*/


//To create new task
// router.post > endpoint > (req, res) > taskController > createfunction > .then (to store result in the variable) > response >

// then go to controller
// then export module export result from task route then create function to find a duplicate data using findOne. to do that, check (name: req.body.name) then use .then ((result, err)) => add another function using

	// if(result !== null && result.name == req.body.name) then return "duplicate task found"

//if result is otherwise, use else and create new task, then task will contain name: req.body.name, then after that return newTask.save. after saving it to the databse use .then to save result in a new variable with parameters. then create another function if result/value is savedErr, console.log the savedErr, else if savedTask yung result, return the result "task created successfully"

// then go to postman to add request

// to delete, change the /:id to the actuall id to display message result

//params are URL in  postman
// add req to the task routes parameter

// to add middleware to connect 

//module.exports = router;