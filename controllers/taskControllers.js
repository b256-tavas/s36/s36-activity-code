// Controllers contains all the functions and business logics of our application

/*const Task = require("../models/Task.js")

module.exports.getAllTasks = () => {

	// .then will store result in a new variable
	return Task.find({}).then(result => {

		return result;
	})
}
*/
// Activity for s36

// Contains all the functions and business logics of our application
// Meaning all the operations it can do will be placed in this file

// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require("../models/Task.js");

module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {

		return result;
	})
};


module.exports.createTask = (requestBody) => {

	return Task.findOne({name: requestBody.name}).then((result, error) => {

		if(result !== null && result.name == requestBody.name) {

			return 'Duplicate Task Found';

		} else {

			let newTask = new Task({

				name: requestBody.name

			})

			return newTask.save().then((savedTask, savedErr) => {

				if(savedErr) {

					console.log(savedErr);

					return 'Task Creation Failed';

				} else {

					return savedTask;

				} 
			})
		}
	})
}

module.exports.deleteTask = (paramsId) => {

	return Task.findByIdAndRemove(paramsId).then((removeTask, err) => {

		if(err) {

			console.log(err)
			return 'Task was not removed';

		} else {

			return 'Task successfully removed';

		}
	})
}

module.exports.updateTask = (paramsId, requestBody) => {

	return Task.findById(paramsId).then((result, err) => {

		if(err) {

			console.log(error);
			return 'Error Found';

		} else {

			result.name = requestBody.name

			return result.save().then((updatedTask, err) => {

				if(err) {

					console.log(err);
					return false;
				
				} else {
					
					return updatedTask;
				}
			})
		}
	})
}

// ACTIVITY SESSION 36

module.exports.getTask = (paramsId, requestBody) => {

	return Task.findById(paramsId).then((result, err) => {

		if(err) {

			console.log(error);
			return 'Error Found';

		} else {

			result.id = requestBody.id

			return result.save().then((getTask, err) => {

				if(err) {

					console.log(err);
					return false;

				} else {

					return getTask;
				}
			})
		}
	})
}


module.exports.updatedTaskStatus = (paramsId, requestBody) => {
	return Task.findById(paramsId).then((result, err) => {

		if(err) {

			console.log(error);
			return 'Error Found';

		} else {
			result.status = requestBody.status

			return result.save().then((updatedTaskStatus, err) => {

				if(err) {

					console.log(err);
					return false;

				} else {
					
					return updatedTaskStatus;
				}
			})
		}
	})
}