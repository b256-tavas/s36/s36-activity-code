// Create the Schema, model and export the file
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

	// field: data_type
	name: String,
	// field: { options }
	status: {
		type: String,
		default: "pending"
	}
});

// "module.exports" is a way for Node JS to treat this value as a "package" that can be used by other files
// ("Task" => collection name "tasks")
module.exports = mongoose.model("Task", taskSchema)